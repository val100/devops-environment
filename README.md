# DevOps Environment

## Description

This docker-compose file is used to deploy an environment with a set of the most used tools in the DevOps world, including:

 - Jenkins
 - Artifactory OS
 - Grafana
 - Graphite (Carbon, Graphite, Statsd)

## Host Requirements

 - git
 - docker
 - docker-compose

## Deployment Steps

 - clone the repository
```
$ git clone https://gitlab.com/sela-devops-course/devops-environment.git
```

 - Run the containers
```
$ docker-compose up -d
```

 - Run the environment containers
```
$ docker-compose up -d
```

 - Check that all the containers are up and running
```
$ docker ps
```
```
Jenkins  ------> Port: 8080
Artifactory ---> Port: 8081
Grafana -------> Port: 3000
Graphite ------> Port: 5000
```

 - Run the following command to retrieve the Jenkins administrator password:
```
$ docker exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword
```

 ![Image 1](/Images/devops-environment-1.PNG)
 
 - Connect to the server in the port 8080 (jenkins) and set the retrieved administraror password:

 ![Image 2](/Images/devops-environment-2.PNG)
 
 - Select "Install suggested plugins" to configure Jenkins:

 ![Image 3](/Images/devops-environment-3.PNG)
 
 - Wait the configuration to finish:

 ![Image 4](/Images/devops-environment-4.PNG)
 
 - Create the first admin user:
```
User: admin
Password: admin
```

 ![Image 5](/Images/devops-environment-5.PNG)
 
 - Click "Start using Jenkins" to finish the configuration

 ![Image 6](/Images/devops-environment-6.PNG)
 
 - Connect to the server in the port 8081 (Artifactory) and click "next" to start the configuration:

 ![Image 7](/Images/devops-environment-7.PNG)
 
 - Configure the admin user:
```
User: admin
Password: admin
```

 ![Image 8](/Images/devops-environment-8.PNG)
 
 - Skip the proxy server configuration:

 ![Image 9](/Images/devops-environment-9.PNG)
 
 - Skip the repository creation:

 ![Image 10](/Images/devops-environment-10.PNG)
 
 - Click "finish" to complete the configuration

 ![Image 11](/Images/devops-environment-11.PNG)
 
 - Connect to the server in the port 3000 (Grafana) and login using the following credentials:
```
User: admin
Password: admin
```

 ![Image 12](/Images/devops-environment-12.PNG)
 
 - Connect to the server in the port 5000 (Graphite) and login using the following credentials:
```
User: root
Password: root
```

 ![Image 13](/Images/devops-environment-13.PNG)

